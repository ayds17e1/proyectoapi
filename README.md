# README #

Google API

Para utilizar la api, obtenga la instancia de GoogleNewsAPI. Solicite el modelo GoogleNewsModel mediante la funcion getModel() de GoogleNewsAPI.
El modelo tiene dos funciones, getNews() y setUrl(url). setUrl añade una url a la colección almacenada. getNews retorna la coleccion de todas las noticias de las urls almacenadas.

Ejemplo de uso:

api = GoogleNewsAPI.getInstance().getModel();

En api se almacenará el objeto de tipo GoogleNewsModel, con los métodos getNews y setUrl.