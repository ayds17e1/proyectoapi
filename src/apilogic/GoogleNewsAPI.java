package apilogic;

public class GoogleNewsAPI {
	
	private static GoogleNewsAPI instance;
	private GoogleNewsModel model;
	
	private GoogleNewsAPI(){
		model = GoogleNewsModelImpl.getInstance();
	}
	
	public static GoogleNewsAPI getInstance(){
		
		if(instance == null){
			instance = new GoogleNewsAPI();
		}
		return instance;
		
	}
	
	public GoogleNewsModel getModel(){
		return model;
	}
	

}
