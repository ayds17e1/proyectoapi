package apilogic;

import java.util.LinkedList;

class GoogleUrlsManager {
	
	private LinkedList<String> urls;
	private static GoogleUrlsManager instance;
	private static String API_KEY = "909dbbe2b77046adb67db6792fd08e90";
	
	private GoogleUrlsManager(){
	
		urls = new LinkedList<String>();
		instance = this;
		
	}
	
	static GoogleUrlsManager getInstance(){
		
		if (instance == null){
			instance = new GoogleUrlsManager();
		}
		return instance;
	
	}
	
	void addUrl(String url){
		urls.add(url+API_KEY);
	}
	
	LinkedList<String> getUrls(){
		return urls;
	}

}
