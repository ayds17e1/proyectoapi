package apilogic;

import apimodel.GoogleAPIFeed;

class GoogleNewsModelImpl implements GoogleNewsModel {

	private GoogleFeedCreator creator;
	private GoogleUrlsManager manager;
	private static GoogleNewsModelImpl instance;

	private GoogleNewsModelImpl() {
		
		creator=GoogleFeedCreator.getInstance();
		manager=GoogleUrlsManager.getInstance();

	}

	public void setURL(String url) {
		
		manager.addUrl(url);
		
	}

	public GoogleAPIFeed[] getNews() {
		
		return creator.getNews();
		
	}
	
	static GoogleNewsModelImpl getInstance(){
		
		if(instance == null){
			instance = new GoogleNewsModelImpl();
		}
		
		return instance;
		
	}

}
