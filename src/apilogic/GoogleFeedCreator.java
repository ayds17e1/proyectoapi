package apilogic;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import apimodel.GoogleAPIFeed;

class GoogleFeedCreator {
	
	private static GoogleFeedCreator instance;
	private GoogleUrlsManager manager;
	
	private GoogleFeedCreator(){
		manager = GoogleUrlsManager.getInstance();
	}
	
	static GoogleFeedCreator getInstance(){
		
		if(instance==null){
			instance = new GoogleFeedCreator();
		}
		return instance;
		
	}
	
	GoogleAPIFeed[] getNews(){
		
		URL url = null;
		
		LinkedList<String> links = manager.getUrls(); 
		
		LinkedList<GoogleAPIFeed> news = new LinkedList<GoogleAPIFeed>();
		
		for (String link : links){
			
			try {

				url = new URL(link);

			} catch (MalformedURLException e) {
				e.printStackTrace();
			}

			JsonReader rdr = null;
			try {

				rdr = Json.createReader(url.openStream());

			} catch (IOException e) {
				e.printStackTrace();
			}

			JsonObject obj = rdr.readObject();
			JsonArray results = obj.getJsonArray("articles");

			for (JsonObject result : results.getValuesAs(JsonObject.class)) {
				news.add(new GoogleAPIFeed(result.getString("title"), result.getString("url")));
			}
			
		}

		GoogleAPIFeed[] newsArray = news.toArray(new GoogleAPIFeed[news.size()]);

		return newsArray;
	}

}
