package apilogic;

import apimodel.GoogleAPIFeed;

public interface GoogleNewsModel {

	public GoogleAPIFeed[] getNews();
	
	public void setURL(String url);
}
